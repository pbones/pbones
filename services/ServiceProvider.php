<?php

namespace pbones\services;

class ServiceProvider
{
    private static $servicesByName = array();
    
    public static function register($name, $serviceObj)
    {
        self::$servicesByName[$name] = $serviceObj;
    }
    
    public static function get($name)
    {
        if (array_key_exists($name, self::$servicesByName)) {
            return self::$servicesByName[$name];
        }
        throw new Exception("Service " . $name . " is not found");
    }
}