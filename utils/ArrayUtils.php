<?php

namespace pbones\utils;

class ArrayUtils
{
    /**
     * Ensures that given array has a sequence of keys (keys in subsequent nested arrays)
     * 
     * @param array $arr
     * @param array $keys
     * @param type $init
     * @param boolean $convertKeysToString If TRUE then converts all keys to string before assinging value in target array. 
     * This might be necessary  in certain circumstances because e.g. when floats are used as keys in arrays
     * they get casted to integers thus loosing their fractional part.
     * @return array& Reference to the deepest array
     */
    public static function &ensureKeys(array &$arr, array $keys = null, $init = array(), $convertKeysToString = false)
    {
        if (is_null($keys) || count($keys) == 0) {
            return $arr;
        }
        
        $subArr = &$arr;
        foreach ($keys as $key) {
            if ($convertKeysToString === true) {
                $key = strval($key);
            }
            if (!array_key_exists($key, $subArr)) {
                $subArr[$key] = array();
            }
            $subArr = &$subArr[$key];
        }
        
        if (count($subArr) == 0) {
            $subArr = $init;
        }
        return $subArr;
    }
    
    /**
     * Same as {@link ensureKeys} but casts all keys to string. This might be necessary
     * in certain circumstances because e.g. when floats are used as keys in arrays
     * they get casted to integers thus loosing their fractional part.
     * 
     * @param array $arr
     * @param array $keys
     * @param type $init
     */
    public static function &ensureStringKeys(array &$arr, array $keys = null, $init = array())
    {
        return self::ensureKeys($arr, $keys, $init, true);
    }
    
    /**
     * Returns an array of values found in the array for specified keys.
     * If specified array doesn't contain one of the specified keys,
     * then the key is ignored (unless $returnNullIfKeyNotExists is true then NULL is included into result array)
     * 
     * @param array $arr
     * @param array $keys
     */
    public static function getMultipleValues(array &$arr, array &$keys, $returnNullIfKeyNotExists = false)
    {
        $ret = [];
        foreach ($keys as $key) {
            if (array_key_exists($key, $arr)) {
                $ret []= $arr[$key];
            } else if ($returnNullIfKeyNotExists === true) {
                $ret []= null;
            }
        }
        return $ret;
    }
    
    /**
     * Creates a new array and sets it's values to values from "source" array using specified "keys".
     * 
     * @param array $keys
     * @param array $source
     * @return array
     */
    public static function createArrayAndMapValues(array &$keys, array &$source)
    {
        $ret = [];
        foreach ($keys as $key) {
            if (array_key_exists($key, $source)) {
                $ret[$key] = $source[$key];
            }
        }
        return $ret;
    }
    
    public static function applyArrayToObject($arr, $object)
    {
        if (empty($arr) || is_null($object)) {
            return;
        }
        foreach ($arr as $key=>$value) {
            if (!property_exists($object, $key)) {
                $object->{$key} = $value;
            }
        }
    }

    public static function renameKeys(&$array, $old_keys, $new_keys)
    {
        if (!is_array($array)) {
            $array == "" ? $array = array() : false;
            return $array;
        }
        else if (!is_array($old_keys) && array_key_exists($old_keys, $array)) {
            $array[$new_keys] = $array[$old_keys];
            unset($array[$old_keys]);
            return $array;
        }
        foreach ($array as &$arr) {
            if (is_array($old_keys)) {
                foreach($new_keys as $k => $new_key) {
                    isset($old_keys[$k]) ? true : $old_keys[$k] = NULL;
                    $arr[$new_key] = isset($arr[$old_keys[$k]]) ? $arr[$old_keys[$k]] : null;
                    unset($arr[$old_keys[$k]]);
                }
            } else {
                $arr[$new_keys] = isset($arr[$old_keys]) ? $arr[$old_keys] : null;
                unset($arr[$old_keys]);
            }
        }
        return $array;
    }
}