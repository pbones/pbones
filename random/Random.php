<?php 

namespace pbones\random;

class Random
{
    
    protected $numericString = null;
    protected $characterString = null;
    protected $mixedString = null;
        
    public function __construct()
    {
        $this->numericString = $this->getNumericString();
        $this->characterString = $this->getCharacterString();
        $this->mixedString = $this->getMixedString();
    }
    
    public function __clone()
    {
        return false;
    }
    
    public function __destruct()
    {
        $this->numericString = null;
        $this->characterString = null;
        $this->mixedString = null;
    }

    /**
     * RandomService::getRandomString( int $length, boolean $numeric, boolean $alpha, boolean $uppercase, boolean $lowercase )
     * @param int $length
     * @param boolean $numeric
     * @param boolean $alpha
     * @param boolean $uppercase
     * @param boolean $lowercase
     * @param string $template
     * @param string $separator
     * @return string
     */
    public function get( $length = false, $numeric = true, $alpha = true, $uppercase = true, $lowercase = true, $template = false, $separator = false )
    {
        
        // Define random properties
        if ( $alpha === false ) {
            $string = $this->numericString;
        } else {
            if ( $numeric === false ) {
                $string = $this->characterString;
            } else {
                $string = $this->mixedString;
            }
        }
        
        if ( $length > 0 && ( $template === false && $separator === false ) ) {
            return $this->getRandomString( $length, $string, $uppercase, $lowercase );
        } else if ( $template !== false && $separator !== false ) {
            $stringArrayTemplate = explode( $separator, $template );
            if ( count( $stringArrayTemplate ) > 0 ) {
                $responce = array();
                for ( $i = 0; $i < count( $stringArrayTemplate ); $i++ ) {
                    array_push( $responce, $this->getRandomString( ( ( is_numeric( $stringArrayTemplate[$i] ) ) ? $stringArrayTemplate[$i] : strlen( $stringArrayTemplate[$i] ) ), $string, $uppercase, $lowercase ) );
                }
                return implode( $separator, $responce );
            }
            else
            {
                return false;
            }
        } else {
            return false;
        }
    
    }
    
    protected function getRandomString( $length = false, $string = null, $uppercase = true, $lowercase = true )
    {
        if ( ! is_null( $string ) ) {
            $stringArray = str_split( $string );
            $response = array();
            // Generating the random string
            for ( $i = 0; $i < $length; $i++ ) {
                if ( $i == 0 ) {
                    array_push( $response, $stringArray[ rand( 0, ( count( $stringArray ) - 1 ) ) ] );
                } else {
                    $a = $response[ ( count( $response ) - 1 ) ];
                    $b = $stringArray[ rand( 0, ( count( $stringArray ) - 1 ) ) ];
                    while( $a == $b ) {
                        $b = $stringArray[ rand( 0, ( count( $stringArray ) - 1 ) ) ];
                    }
                    array_push( $response, $b );
                }
            }
                
            // Checking to uppercase and lowercase
            if ( ( $uppercase === true && $lowercase === true ) || ( $uppercase === false && $lowercase === false ) ) {
                for ( $i = 0; $i < count( $response ); $i++ ) {
                    $response[$i] = ( ( rand( 0,1 ) == 0 ) ) ? mb_strtoupper( $response[$i], 'UTF-8' ) : mb_strtolower( $response[$i], 'UTF-8' );
                }
                return implode( "", $response );
            } else {
                if ( $uppercase === true && $lowercase === false ) {
                    $response = implode( "", $response );
                    return mb_strtoupper( $response, 'UTF-8' );
                } else {
                    $response = implode( "", $response );
                    return mb_strtolower( $response, 'UTF-8' );
                }
            }
        }
        else {
            return false;
        }
            
    }
    
    /**
     * Get alpha and numeric random string
     * @return string
     */
    protected function getMixedString()
    {
        return hash( 'sha512', $this->getNumericString() ) . hash( 'sha512', time() - rand(0,10000) ) . hash( 'sha512', md5( time() - rand(10000, 100000) ) ) . md5( time() ) . hash( 'sha512', rand( 1, 10000000 ) );
    }
    
    /**
     * Get only numeric string
     * @return string
     */
    protected function getNumericString()
    {
        return ( time() - rand( 1,1000 ) ) . rand(0,9) . rand(10,100) . rand(100,1000) . rand( 1000, 10000 ) . rand( 10000, 100000 ) . rand( 100000, 1000000 ) . rand( 1000000, 10000000 ) . rand( 10000000, 1000000000 ) . time();
    }
    
    /**
     * Get only alpha string
     * @return string
     */
    protected function getCharacterString()
    {
        $mixedString = str_split( $this->getMixedString() );
        $characterString = array();
        for ( $i = 0; $i < count( $mixedString ); $i++ ) {
            if ( ! is_numeric( $mixedString[$i] ) ) {
                array_push( $characterString, $mixedString[$i] );
            }
        }
        return implode( "", $characterString );
    }
    
    
}