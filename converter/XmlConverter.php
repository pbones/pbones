<?php

namespace pbones\converter;
use DOMDocument;

class XmlConverter {

    public $version = '1.0';
    public $encoding = 'utf-8';
    public $node = 'node';
    public $format = true;
    
    public function __construct()
    {
        
    }
    
    public function __destruct()
    {
        
    }
    
    public function __clone()
    {
        return false;
    }
    
    public function convert( $array = false, $node = null )
    {
        $node = ( is_null( $node ) ) ? $this->node : $node;
        if ( ( is_array( $array ) || is_object( $array ) ) && count( $array ) > 0 ) {
            $array = (array) $array;
            if ( count( $array ) == 1 ) {
                reset( $array );
                $arrayKey = key( $array );
                $rootKey = ( is_numeric( $arrayKey ) ) ? $node : $arrayKey;
                $array = array_shift( $array );
            } else if ( count( $array ) > 1 ) {
                $rootKey = $node;
            }
            $dom = new DOMDocument( $this->version, $this->encoding );
            $dom->formatOutput = ( $this->format === true ) ? true : false;
            $root = $dom->createElement( $rootKey );
            $dom->appendChild( $root );
            if ( is_array( $array ) || is_object( $array ) ) {
                $array = (array) $array;
                foreach ( $array as $key => $value ) {
                    $name = ( is_numeric( $key ) ) ? $node : (string) $key;
                    $node = $dom->createElement($name);
                    //$node = $dom->createElement(str_replace(' ', '', $name));
                    $this->appendData( $dom, $node, $value, $node );
                    $root->appendChild( $node );
                }
            } else {
                $data = (string) $array;
                if ( preg_match( "/[><]/", $data ) ) {
                    $root->appendChild( $dom->createCDATASection( $data ) );
                } else {
                    $root->appendChild( $dom->createTextNode( trim( $data ) ) );
                }
            }
            return $dom->saveXML();
        }
        return null;
    }
    
    private function appendData( $dom = false, $element = false, $data = false, $node )
    {
        $node = ( is_null( $node ) ) ? $this->node : $node;
        if ( $element ) {
            if ( is_bool( $data ) ) {
                $element->appendChild( $dom->createTextNode( ( ( $data === false ) ? 'false' : 'true' ) ) );
            } else if ( is_null( $data ) ) {
                $element->appendChild( $dom->createTextNode('null') );
            } else if ( is_array( $data ) || is_object( $data ) ) {
                $data = (array) $data;
                foreach ( $data as $key => $value ) {
                    $name = ( is_numeric( $key ) || is_int( $key ) || is_object( $key ) ) ? $this->node : $key;
                    $node = $dom->createElement(str_replace(' ', '', $name));
                    $this->appendData( $dom, $node, $value, $node );
                    $element->appendChild( $node );
                }
            } else {
                $data = (string) $data;
                if ( preg_match( "/[><]/", $data ) ) {
                    $element->appendChild( $dom->createCDATASection( $data ) );
                } else {
                    $element->appendChild( $dom->createTextNode( trim( $data ) ) );
                }
            }
        }
    }

}