<?php

namespace pbones\test;

abstract class BareTestBase
{
    protected function assertEquals($expected, $actual)
    {
        if ($expected != $actual) {
            throw new AssertionFailureException("Expected is not equal to actual (" . $expected . " != " . $actual . ")");
        }
    }
}