<?php

namespace pbones\query;

abstract class QueryExprOperator
{
    const AND_OP = "AND";
    const OR_OP = "OR";
    const EQUAL = "=";
    const NULLSAFEEQUAL = "<=>";
    const NOT_EQUAL = "<>";
    const LESS_THAN = "<";
    const GREATER_THAN = ">";
    const LESS_THAN_OR_EQUAL = "<=";
    const GREATER_THAN_OR_EQUAL = ">=";
    const IS_NULL = "IS NULL";
    const IS_NOT_NULL = "IS NOT NULL";
    const ADD = "+";
    const SUB = "-";
    const MULT = "*";
    const DIV = "/";
    const LIKE = "LIKE";
    const NOT_LIKE = "NOT LIKE";
    const IN = "IN";
    const NOT_IN = "NOT IN";
}
