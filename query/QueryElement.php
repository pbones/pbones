<?php

namespace pbones\query;

interface QueryElement 
{
    /**
     * Appends a SQL portion with "declaration" of the unit to the specified string.
     * "Declaration" means that in case, for example, of an aliased column this method will append
     * <code>`column_name` AS `alias`</code> instead of just <code>`alias`</code> as in case of 
     * {@link QueryAssemblyUnit#assembleUsageSql(StringBuilder, List)}.
     * 
     * @param $str
     * @param $parameterValues
     */
    public function assembleDeclarationSql(&$str, array &$parameterValues);
    
    /**
     * Appends a SQL portion with "usage" of the unit to the specified string.
     * "Usage" means that in case, for example, of an aliased column this method will append
     * <code>`alias`</code>.
     * 
     * @param $str
     * @param $parameterValues
     */
    public function assembleUsageSql(&$str,  array &$parameterValues);
}
