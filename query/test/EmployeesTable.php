<?php
namespace pbones\query\test;

use pbones\query\DbTable;
use pbones\query\Field;

function employeesTable()
{
    return EmployeesTable::$Instance;
}

class EmployeesTable extends DbTable
{
    /**
     *
     * @var EmployeesTable     
     */
    public static $Instance;
    
    /**
     * @var Field
     */
    public $last_name;
    /**
     * @var Field
     */
    public $department_id;
    /**
     * @var Field
     */
    public $salary;
    
    public function __construct($alias = null)
    {
        parent::__construct("employees", $alias);
        
        $this->last_name = new Field('last_name', $this);
        $this->department_id = new Field('department_id', $this);
        $this->salary = new Field('salary', $this);
    }
    
    public function asAlias($alias)
    {
        return new EmployeesTable($alias);
    }
}

EmployeesTable::$Instance = new EmployeesTable();
