<?php

/**
 */
class DeleteQueryTest extends PHPUnit_Framework_TestCase
{
    public function testSimpleDelete()
    {
        $employees = new EmployeesTable();
        
        $deleteQuery = new DeleteQuery();
        $deleteQuery
                ->delete()
                ->from($employees)
                ->where($employees->last_name->equal("Smith"));
        
        $str = "";
        $params = array();
        $deleteQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "DELETE FROM `employees` WHERE `employees` .`last_name` = ? ",
                $str);
    }
    
    public function testMultiDelete()
    {        
        $employees1 = new EmployeesTable();
        $employees1 = $employees1->asAlias("emp1");
        $employees2 = new EmployeesTable();
        $employees2 = $employees2->asAlias("emp2");
        
        $deleteQuery = new DeleteQuery();
        $deleteQuery
                ->delete($employees1)
                ->from($employees1)
                ->innerJoin($employees2, 
                        $employees1->department_id->equal($employees2->department_id)
                        ->andExpr($employees1->last_name->equal($employees2->last_name)))
                ->where($employees1->last_name->equal("Smith"));
        
        $str = "";
        $params = array();
        $deleteQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "DELETE `emp1` FROM `employees` AS `emp1` INNER JOIN `employees` AS `emp2` ON `emp1` .`department_id` = `emp2` .`department_id` AND `emp1` .`last_name` = `emp2` .`last_name` WHERE `emp1` .`last_name` = ? ",
                $str);
    }
}
