<?php

class InsertQueryTest extends PHPUnit_Framework_TestCase
{
    public function testSimpleInsert()
    {
        $employees = new EmployeesTable();
        
        $insertQuery = new InsertQuery();
        $insertQuery
            ->insertInto($employees, 
                $employees->department_id, 
                $employees->last_name, 
                $employees->salary
            )
            ->values(10, "Shtulz", 800);
        
        $str = "";
        $params = array();
        $insertQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "INSERT INTO `employees` (`employees` .`department_id` , `employees` .`last_name` , `employees` .`salary` ) VALUES (? , ? , ? ) ",
                $str);
        $this->assertEquals(3, count($params));
    }
    
    public function testInsertWithoutColumns()
    {        
        $employees = new EmployeesTable();
        
        $insertQuery = new InsertQuery();
        $insertQuery
            ->insertInto($employees)
            ->values(10, "Shtulz", 800);
        
        $str = "";
        $params = array();
        $insertQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "INSERT INTO `employees` VALUES (? , ? , ? ) ",
                $str);
    }
    
    public function testInsertIntoSelect()
    {        
        $employees = new EmployeesTable();
        
        $insertQuery = new InsertQuery();
        $insertQuery
            ->insertInto($employees)
            ->select(
                QueryExpr::val(20)->asAlias("department_id"),
                $employees->last_name,
                $employees->salary->mult(2)
            )
            ->from($employees)
            ->where(
                $employees->department_id->equal(10)
                ->andExpr($employees->salary->lessThan(100))
            );
        
        $str = "";
        $params = array();
        $insertQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "INSERT INTO `employees` SELECT ? AS `department_id` , `employees` .`last_name` , `employees` .`salary` * ? FROM `employees` WHERE `employees` .`department_id` = ? AND `employees` .`salary` < ? ",
                $str);
    }
    
    public function testInsertWithOnDuplicateKeyUpdate()
    {
        $employees = new EmployeesTable();
        
        $insertQuery = new InsertQuery();
        $insertQuery
            ->insertInto($employees)
            ->select(
                QueryExpr::val(20)->asAlias("department_id"),
                $employees->last_name,
                $employees->salary->mult(2)
            )
            ->from($employees)
            ->where(
                $employees->department_id->equal(10)
                ->andExpr($employees->salary->lessThan(100))
            )
            ->onDuplicateKeyUpdate()
            ->set($employees->salary, QueryExpr::values($employees->salary));
        
        $str = "";
        $params = array();
        $insertQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "INSERT INTO `employees` SELECT ? AS `department_id` , `employees` .`last_name` , `employees` .`salary` * ? FROM `employees` WHERE `employees` .`department_id` = ? AND `employees` .`salary` < ? ON DUPLICATE KEY UPDATE `employees` .`salary` = VALUES(`employees` .`salary` ) ",
                $str);
    }
    
    public function testInsertQueryAfterCreationModification()
    {
        $employees = new EmployeesTable();
        
        $insertQuery = new InsertQuery();
        $insertQuery
            ->insertInto($employees)
            ->select(
                QueryExpr::val(20)->asAlias("department_id"),
                $employees->last_name,
                $employees->salary->mult(2)
            )
            ->from($employees)
            ->where(
                $employees->department_id->greaterThan(10)
                ->andExpr($employees->salary->lessThan(100))
            )
            ->onDuplicateKeyUpdate()
            ->set($employees->salary, QueryExpr::values($employees->salary));
        
        $insertQuery->whereExpr = $insertQuery->whereExpr->andExpr($employees->department_id->lessThan(30));
        
        $str = "";
        $params = array();
        $insertQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "INSERT INTO `employees` SELECT ? AS `department_id` , `employees` .`last_name` , `employees` .`salary` * ? FROM `employees` WHERE `employees` .`department_id` > ? AND `employees` .`salary` < ? AND `employees` .`department_id` < ? ON DUPLICATE KEY UPDATE `employees` .`salary` = VALUES(`employees` .`salary` ) ",
                $str);
    }
}