<?php

use pbones\query\SelectQuery;
use pbones\query\QueryExpr;
use pbones\query\test\EmployeesTable;

class SelectQueryTest extends PHPUnit_Framework_TestCase
{
    public function testSelectNull()
    {
        $selectQuery = new SelectQuery();
        $selectQuery
                ->select(QueryExpr::val(null));
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "SELECT ? ",
                $str);
        $this->assertEquals(1, count($params));
        $this->assertEquals(null, $params[0]);
    }
    
    public function testSimpleSelectWithAlias()
    {
        $employees = new EmployeesTable();
        
        $deptExpr = $employees->department_id->greaterThan(2)->asAlias("dept_gt_2");
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                QueryExpr::val(1)->asAlias("some_const"),
                $employees->department_id, 
                $deptExpr, 
                $employees->last_name
            )
            ->from($employees)
            ->where($employees->department_id->equal(3));        
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "SELECT ? AS `some_const` , `employees` .`department_id` , `employees` .`department_id` > ? AS `dept_gt_2` , `employees` .`last_name` FROM `employees` WHERE `employees` .`department_id` = ? ",
                $str);
        $this->assertEquals(3, count($params));
    }
    
    public function testSelectInnerJoin()
    {
        $employees1 = new EmployeesTable("emp1");
        $employees2 = new EmployeesTable("emp2");
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                $employees1->department_id->asAlias("dept1"), 
                $employees2->department_id->asAlias("dept2")
            )
            ->from($employees1)
            ->innerJoin($employees2, $employees1->last_name->equal($employees2->last_name))
            ->where(
                $employees1->department_id->equal(3)
                ->andExpr($employees2->department_id->equal(4))
            );
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);

//        echo $str;
        $this->assertEquals(
                "SELECT `emp1` .`department_id` AS `dept1` , `emp2` .`department_id` AS `dept2` FROM `employees` AS `emp1` INNER JOIN `employees` AS `emp2` ON `emp1` .`last_name` = `emp2` .`last_name` WHERE `emp1` .`department_id` = ? AND `emp2` .`department_id` = ? ",
                $str);
    }
    
    public function testSelectLeftJoin()
    {
        $employees1 = new EmployeesTable("emp1");
        $employees2 = new EmployeesTable("emp2");
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                $employees1->department_id->asAlias("dept1"), 
                $employees2->department_id->asAlias("dept2")
            )
            ->from($employees1)
            ->leftJoin($employees2, $employees1->last_name->equal($employees2->last_name))
            ->where(
                $employees1->department_id->equal(3)
                ->andExpr($employees2->department_id->equal(4))
            );
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);

//        echo $str;
        $this->assertEquals(
                "SELECT `emp1` .`department_id` AS `dept1` , `emp2` .`department_id` AS `dept2` FROM `employees` AS `emp1` LEFT JOIN `employees` AS `emp2` ON `emp1` .`last_name` = `emp2` .`last_name` WHERE `emp1` .`department_id` = ? AND `emp2` .`department_id` = ? ",
                $str);
    }
    
    public function testSelectCountWithGroupByAndHaving()
    {        
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                $employees->department_id,
                QueryExpr::count()
            )
            ->from($employees)
            ->groupBy($employees->department_id)
            ->having(QueryExpr::count()->greaterThan(5));
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "SELECT `employees` .`department_id` , COUNT(*) FROM `employees` GROUP BY `employees` .`department_id` HAVING COUNT(*) > ? ",
                $str);
    }
    
    public function testSelectWithOrderByDesc()
    {        
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                $employees->department_id
            )
            ->from($employees)
            ->orderBy(QueryExpr::desc($employees->department_id));
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
        $this->assertEquals(
                "SELECT `employees` .`department_id` FROM `employees` ORDER BY `employees` .`department_id` DESC ",
                $str);
    }
    
    public function testSelectWithLimitOffset()
    {        
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select(
                $employees->department_id
            )
            ->from($employees)
            ->limit(5)
            ->offset(3);
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "SELECT `employees` .`department_id` FROM `employees` LIMIT 5 OFFSET 3 ",
                $str);
    }
    
    public function testOffsetGenerationBug()
    {
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select()
            ->from( $employees )
            ->where( $employees->department_id->equal( 5 )->andExpr( $employees->last_name->equal( 'Smith' )->andExpr( $employees->salary->equal( '0' ) ) ) )
            ->limit( 1 )
            ->offset( 0 )
            ;
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "SELECT * FROM `employees` WHERE `employees` .`department_id` = ? AND `employees` .`last_name` = ? AND `employees` .`salary` = ? LIMIT 1 OFFSET 0 ",
                $str);
    }
    
    public function testSelectWithIn()
    {
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select()
            ->from( $employees )
            ->where( $employees->department_id->in(array(1, 2)))
            ;
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "SELECT * FROM `employees` WHERE `employees` .`department_id` IN (? , ? ) ",
                $str);
    }
    
    public function testFalseAsValue()
    {
        $employees = new EmployeesTable();
        
        $selectQuery = new SelectQuery();
        $selectQuery
            ->select()
            ->from( $employees )
            ->where( $employees->department_id->equal(false))
            ;
        
        $str = "";
        $params = array();
        $selectQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "SELECT * FROM `employees` WHERE `employees` .`department_id` = ? ",
                $str);
        $this->assertEquals($params[0], 0);
    }
}
