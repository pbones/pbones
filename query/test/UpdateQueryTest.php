<?php

/**
 */
class UpdateQueryTest extends PHPUnit_Framework_TestCase
{
    public function testSimpleUpdate()
    {        
        $employees = new EmployeesTable();
        
        $updateQuery = new UpdateQuery();
        $updateQuery
                ->update($employees)
                ->set($employees->salary, 500)
                ->where($employees->last_name->equal("Smith"));
        
        $str = "";
        $params = array();
        $updateQuery->assembleUsageSql($str, $params);
        
//        echo $str;
        
        $this->assertEquals(
                "UPDATE `employees` SET `employees` .`salary` = ? WHERE `employees` .`last_name` = ? ",
                $str);
        $this->assertEquals(2, count($params));
    }
}
