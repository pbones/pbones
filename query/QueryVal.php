<?php

namespace pbones\query;

/**
 */
class QueryVal extends QueryExpr {
    
    private $value;
    
    public function __construct($value)
    {
        parent::__construct();
        if (is_bool($value)) {
            $this->value = $value === true ? 1 : 0;
        } else {
            $this->value = $value;
        }
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $str .= "? ";
        array_push($parameterValues, $this->value);
    }
}
