<?php

namespace pbones\query;

class QuerySortExpr extends QueryExpr 
{
    private $expr;
    private $direction;
    
    public function __construct(QueryExpr $expr, $direction)
    {
        parent::__construct();
        
        $this->expr = $expr;
        $this->direction = $direction;
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $this->expr->assembleUsageSql($str, $parameterValues);
        $str .= $this->direction . " ";
    }
}
