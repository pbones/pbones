<?php

namespace pbones\query;

class SelectQuery extends JoinsAndWhereQueryBase
{
    /**
     *
     * @var array 
     */
    public $selectItems = array();
    private $selectAll = false;
    /**
     *
     * @var array 
     */
    public $groupByExprs = array();
    /**
     *
     * @var array 
     */
    public $orderByExprs = array();
    /**
     * @var QueryExpr
     */
    public $havingExpr;
    public $limitCount;
    public $limitOffset;
    
    public function select()
    {
        if (func_num_args() === 0) {
            $this->selectAll = true;
        }
        else {
            foreach (func_get_args() as $arg) {
                $arg = $arg instanceof QueryExpr ? $arg : new QueryVal($arg);
                array_push($this->selectItems, $arg);
            }
        }
        return $this;
    }
    
    public function from(DbTable $model)
    {
        $this->firstTable = $model;
        return $this;
    }
    
    public function groupBy()
    {
        $this->groupByExprs = func_get_args();
        return $this;
    }
    
    public function orderBy()
    {
        $this->orderByExprs = func_get_args();
        return $this;
    }
    
    public function having(QueryExpr $expr)
    {
        $this->havingExpr = $expr;
        return $this;
    }
    
    public function limit($count)
    {
        $this->limitCount = $count;
        return $this;
    }
    
    public function offset($offset)
    {        
        $this->limitOffset = $offset;
        return $this;
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        if ($this->selectItems !== null && $this->selectItems !== array()) {
            $str .= "SELECT ";
            $first = true;
            foreach ($this->selectItems as $expr) {
                if ($first === false) {
                    $str .= ", ";
                }
                else {
                    $first = false;
                }
                $expr->assembleDeclarationSql($str, $parameterValues);
            }
        }
        else if ($this->selectAll === true) {
            $str .= "SELECT * ";
        }
        
        if ($this->firstTable !== null) {
            $str .= "FROM ";
        }
        $this->assembleTableAndJoinsSql($str, $parameterValues);
        
        $this->assembleWhereSql($str, $parameterValues);

        if ($this->groupByExprs !== null && $this->groupByExprs !== array()) {
            $str .= "GROUP BY ";
            $first = true;
            foreach ($this->groupByExprs as $expr) {
                if ($first === false) {
                    $str .= ", ";
                }
                else {
                    $first = false;
                }
                $expr->assembleUsageSql($str, $parameterValues);
            }
        }

        if ($this->orderByExprs !== null && $this->orderByExprs !== array()) {
            $str .= "ORDER BY ";
            $first = true;
            foreach ($this->orderByExprs as $expr) {
                if ($first === false) {
                    $str .= ", ";
                }
                else {
                    $first = false;
                }
                $expr->assembleUsageSql($str, $parameterValues);
            }
        }
        
        if ($this->havingExpr !== null) {
            $str .= "HAVING ";
            $this->havingExpr->assembleUsageSql($str, $parameterValues);
        }
        
        if ($this->limitCount !== null) {
            $str .= "LIMIT " . $this->limitCount . " ";
        }
        
        if ($this->limitOffset !== null) {
            $str .= "OFFSET " . $this->limitOffset . " ";
        }
    }
}
