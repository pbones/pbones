<?php

namespace pbones\query;

class Setter
{
    private $field;
    private $value;
    
    public function __construct(Field $field, $value)
    {        
        $this->setField($field);
        $this->setValue($value);
    }
    
    public function getField()
    {
        return $this->field;
    }
    
    public function setField(Field $field)
    {
        $this->field = $field;
    }
    
    public function getValue()
    {
        return $this->value;
    }
    
    public function setValue($value)
    {
        $value = $value instanceof QueryExpr ? $value : new QueryVal($value);
        $this->value = $value;
    }
}