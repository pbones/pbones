<?php

namespace pbones\query;

class QueryFuncExpr extends QueryExpr
{
    private $function;
    private $args = array();
    
    public function __construct($function, array $args = array())
    {
        parent::__construct();
        $this->function = $function;
        $this->args = $args;
        for ($i = 0; $i < count($this->args); $i++) {
            if (! ($this->args[$i] instanceof QueryExpr)) {
                $this->args[$i] = new QueryVal($this->args[$i]);
            }
        }
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $str .= $this->function;
        if ($this->function != QueryExprFunctions::COUNT_STAR) {
            $str .= "(";
            $first = true;
            foreach ($this->args as $expr) {
                if ($first === false) {
                    $str .= ", ";
                }
                else {
                    $first = false;
                }
                $expr->assembleUsageSql($str, $parameterValues);
            }
            $str .= ")";
        }
        $str .= " ";
    }
}