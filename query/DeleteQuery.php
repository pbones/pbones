<?php

namespace pbones\query;

/**
 */
class DeleteQuery extends JoinsAndWhereQueryBase
{
    private $tablesToDelete;
    
    public function delete()
    {
        $this->tablesToDelete = func_get_args();
        return $this;
    }
    
    public function from(DbTable $table)
    {
        $this->firstTable = $table;
        return $this;
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $str .= "DELETE ";
        
        $first = true; 
        foreach ($this->tablesToDelete as $table) {
            if ($first) {
                $first = false;
            }
            else {
                $str .= ", ";
            }
            $str .= $table->assembleUsageSql($str, $parameterValues);
        }
        
        $str .= "FROM ";
        
        $this->assembleTableAndJoinsSql($str, $parameterValues);
        $this->assembleWhereSql($str, $parameterValues);
    }
}
