<?php

namespace pbones\query;

abstract class QueryExprFunctions
{
    const ABS = "ABS";
    const MIN = "MIN";
    const MAX = "MAX";
    const COUNT = "COUNT";
    const COUNT_STAR = "COUNT(*)";
    const SUM = "SUM";
    const FOUND_ROWS = "FOUND_ROWS";
    const CONCAT = "CONCAT";
    const IF_EXPR = "IF";
    const VALUES = "VALUES";
    const SQRT = "SQRT";
    const LOWER = "LOWER";
    const UPPER = "UPPER";
    const NOW = "NOW";
    const DATE = "DATE";
    const YEAR = "YEAR";
    const MONTH = "MONTH";
    const DATE_ADD = "DATE_ADD";
    const DATE_SUB = "DATE_SUB";
    const COALESCE = "COALESCE";
    const GREATEST = "GREATEST";
    const LEAST = "LEAST";
    const ROUND = "ROUND";
    const ROW = "ROW";
    const REPLACE = "REPLACE";
    const EXISTS = "EXISTS";
    const NOT_EXISTS = "NOT EXISTS";
    const UNIX_TIMESTAMP = "UNIX_TIMESTAMP";
    const CONCAT_WS = "CONCAT_WS";
    const CURDATE = "CURDATE";
    const BIT_AND = "BIT_AND";
    const BIT_OR = "BIT_OR";
}