<?php

namespace pbones\query;

/**
 *
 */
class QueryExpr implements QueryElement
{
    private $leftExpr;
    private $op;
    private $rightExpr;
    
    public function __construct($left = null, $op = null, $right = null)
    {
        if ($left !== null) {
            $left = $left instanceof QueryExpr ? $left : new QueryVal($left);
        }
        if ($right !== null) {
            if (is_array($right)) {
                for ($i = 0; $i < count($right); $i++) {
                    $right[$i] = $right[$i] instanceof QueryExpr ? $right[$i] : new QueryVal($right[$i]);
                }
            } else {
                $right = $right instanceof QueryExpr ? $right : new QueryVal($right);
            }
        }
        
        $this->leftExpr = $left;
        $this->op = $op;
        $this->rightExpr = $right;
    }
    
    public function assembleDeclarationSql(&$str, array &$parameterValues) {
        return $this->assembleUsageSql($str, $parameterValues);
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues) {
        $addParen = $this->op == QueryExprOperator::ADD
                || $this->op == QueryExprOperator::SUB
                || $this->op == QueryExprOperator::OR_OP;
        
        if ($addParen === true) {
            $str .= "("; 
        }
        $this->leftExpr->assembleUsageSql($str, $parameterValues);
        if ($this->op !== null) {
            $str .= $this->op . " ";
        }
        if ($this->rightExpr !== null) {
            if ($this->op === QueryExprOperator::IN || $this->op === QueryExprOperator::NOT_IN) {
                $this->renderInOp($str, $parameterValues);
            } else {
                $this->rightExpr->assembleUsageSql($str, $parameterValues);
            }
        }
        if ($addParen) {
            $str .= ") "; 
        }
    }
    
    private function renderInOp(&$str, array &$parameterValues) {
        $str .= "("; 
        $first = true;
        foreach ($this->rightExpr as $expr) {
            if ($first === true) {
                $first = false;
            } else {
                $str .= ", ";
            }
            $expr->assembleUsageSql($str, $parameterValues);
        }
        $str .= ") "; 
    }
    
    /**
     * Returns a new {@link AliasedExpr} expression wrapped around this expression with specified alias.
     * See {@link AliasedExpr} regarding details about rendering details.
     *
     * @param string $alias Alias of the expression (e.g. <code>min_date</code> in case of <code>MIN(date) AS min_date</code> expression)
     * @return AliasedExpr
     */
    public function asAlias($alias)
    {
        return new AliasedExpr($this, $alias);
    }
    
    public function equal($expr)
    {
        return new QueryExpr($this, QueryExprOperator::EQUAL, $expr);
    }
    
    public function notEqual($expr)
    {
        return new QueryExpr($this, QueryExprOperator::NOT_EQUAL, $expr);
    }
    
    public function isNull()
    {
        return new QueryExpr($this, QueryExprOperator::IS_NULL);
    }
    
    public function isNotNull()
    {
        return new QueryExpr($this, QueryExprOperator::IS_NOT_NULL);
    }
    
    public function in(array $expr)
    {
        return new QueryExpr($this, QueryExprOperator::IN, $expr);
    }
    
    public function notIn(array $expr)
    {
        return new QueryExpr($this, QueryExprOperator::NOT_IN, $expr);
    }
    
    public function like($expr)
    {
        return new QueryExpr($this, QueryExprOperator::LIKE, $expr);
    }
    
    public function notLike($expr)
    {
        return new QueryExpr($this, QueryExprOperator::NOT_LIKE, $expr);
    }
    
    public function lessThan($expr)
    {
        return new QueryExpr($this, QueryExprOperator::LESS_THAN, $expr);
    }
    
    public function lessThanOrEqual($expr)
    {
        return new QueryExpr($this, QueryExprOperator::LESS_THAN_OR_EQUAL, $expr);
    }
    
    public function greaterThan($expr)
    {
        return new QueryExpr($this, QueryExprOperator::GREATER_THAN, $expr);
    }
    
    public function greaterThanOrEqual($expr)
    {
        return new QueryExpr($this, QueryExprOperator::GREATER_THAN_OR_EQUAL, $expr);
    }
    
    public function mult($expr)
    {
        return new QueryExpr($this, QueryExprOperator::MULT, $expr);
    }
    
    public function div($expr)
    {
        return new QueryExpr($this, QueryExprOperator::DIV, $expr);
    }
    
    public function add($expr)
    {
        return new QueryExpr($this, QueryExprOperator::ADD, $expr);
    }
    
    public function sub($expr)
    {
        return new QueryExpr($this, QueryExprOperator::SUB, $expr);
    }
    
    public function andExpr($expr)
    {
        return new QueryExpr($this, QueryExprOperator::AND_OP, $expr);
    }
    
    public function orExpr($expr)
    {
        return new QueryExpr($this, QueryExprOperator::OR_OP, $expr);
    }
    
    public static function val($value)
    {
        return new QueryVal($value);
    }
    
    public static function count(QueryExpr $expr = null)
    {
        if ($expr == null) {
            return new QueryFuncExpr(QueryExprFunctions::COUNT_STAR);
        }
        return new QueryFuncExpr(QueryExprFunctions::COUNT, array($expr));
    }
    
    public static function sum(QueryExpr $expr = null)
    {
        if ($expr == null) {
            return new QueryFuncExpr(QueryExprFunctions::SUM);
        }
        return new QueryFuncExpr(QueryExprFunctions::SUM, array($expr));
    }
    
    public static function now()
    {
        return new QueryFuncExpr(QueryExprFunctions::NOW);
    }
    
    public static function values(Field $field)
    {
        return new QueryFuncExpr(QueryExprFunctions::VALUES, array($field));
    }
    
    public static function unixTimestamp(QueryExpr $expr = null)
    {
        if ($expr == null) {
            return new QueryFuncExpr(QueryExprFunctions::UNIX_TIMESTAMP);
        }
        return new QueryFuncExpr(QueryExprFunctions::UNIX_TIMESTAMP, array($expr));
    }
    
    public static function concat()
    {
        if (func_num_args() === 0) {
            return new QueryFuncExpr(QueryExprFunctions::CONCAT);
        }
        return new QueryFuncExpr(QueryExprFunctions::CONCAT, func_get_args());
    }
    
    public static function ifExpr($cond, $then, $else)
    {
        return new QueryFuncExpr(QueryExprFunctions::IF_EXPR, array($cond, $then, $else));
    }
    
    public static function concatWs()
    {
        if (func_num_args() === 0) {
            return new QueryFuncExpr(QueryExprFunctions::CONCAT_WS);
        }
        return new QueryFuncExpr(QueryExprFunctions::CONCAT_WS, func_get_args());
    }
    
    public static function curdate()
    {
        if (func_num_args() === 0) {
            return new QueryFuncExpr(QueryExprFunctions::CURDATE);
        }
        return new QueryFuncExpr(QueryExprFunctions::CURDATE, func_get_args());
    }

    public static function bitAnd(QueryExpr $expr)
    {
        return new QueryFuncExpr(QueryExprFunctions::BIT_AND, array($expr));
    }

    public static function bitOr(QueryExpr $expr)
    {
        return new QueryFuncExpr(QueryExprFunctions::BIT_OR, array($expr));
    }

    public static function desc(QueryExpr $expr)
    {
        return new QuerySortExpr($expr, "DESC");
    }
    
    public static function asc(QueryExpr $expr)
    {
        return new QuerySortExpr($expr, "ASC");
    }
}
