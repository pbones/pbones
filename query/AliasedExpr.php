<?php

namespace pbones\query;
/**
 */
class AliasedExpr extends QueryExpr
{
    private $alias;
    private $expr;
    
    public function __construct(QueryExpr $expr, $alias)
    {
        $this->expr = $expr;
        $this->alias = $alias;
    }
    
    public function assembleDeclarationSql(&$str, array &$parameterValues)
    {
        $this->expr->assembleDeclarationSql($str, $parameterValues);
        $str .= "AS `" . $this->alias . "` ";
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $str .= "`" . $this->alias . "` ";
    }
}
