<?php

namespace pbones\query;

class Field extends QueryExpr
{
    private $fieldName;
    private $model;
    
    public function __construct($fieldName, DbTable $model)
    {
        $this->fieldName = $fieldName;
        $this->model = $model;
    }
    
    public function assembleDeclarationSql(&$str, array &$parameterValues)
    {
        $this->assembleUsageSql($str, $parameterValues);
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $this->model->assembleUsageSql($str, $parameterValues);
        $str .= ".`" . $this->fieldName . "` ";
    }    
}
