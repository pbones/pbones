<?php

namespace pbones\query;

class InsertQuery extends SelectQuery
{
    private $intoModel;
    public $fields = array();
    public $values = array();
    private $isOnDuplicateKeyUpdate = false;
    private $setters = array();
    
    public function insertInto(DbTable $model)
    {
        $this->intoModel = $model;
        $this->fields = func_get_args();
        array_shift($this->fields);
        
        return $this;
    }
    
    public function values()
    {
        foreach (func_get_args() as $arg) {
            $arg = $arg instanceof QueryExpr ? $arg : new QueryVal($arg);
            array_push($this->values, $arg);
        }  
        return $this;
    }
    
    public function onDuplicateKeyUpdate()
    {
        $this->isOnDuplicateKeyUpdate = true;
        return $this;
    }
    
    public function set(Field $field, $valueExpr)
    {
        array_push($this->setters, new Setter($field, $valueExpr));        
        return $this;
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues) {
        $str .= "INSERT INTO ";
        
        $this->intoModel->assembleUsageSql($str, $parameterValues);
        
        if ($this->fields !== array()) {
            $str .= "(";
            $first = true;
            foreach ($this->fields as $field) {
                if ($first) {
                    $first = false;
                }
                else {
                    $str .= ", ";
                }
                $field->assembleUsageSql($str, $parameterValues);
            }
            $str .= ") ";
        }
        
        if ($this->values !== array()) {
            $str .= "VALUES (";
            $first = true;
            foreach ($this->values as $valueExpr) {
                if ($first) {
                    $first = false;
                }
                else {
                    $str .= ", ";
                }
                $valueExpr->assembleUsageSql($str, $parameterValues);
            }
            $str .= ") ";
        }
        else {
            parent::assembleUsageSql($str, $parameterValues);
        }
        
        if ($this->isOnDuplicateKeyUpdate === true && $this->setters !== array()) {
            $str .= "ON DUPLICATE KEY UPDATE ";
            $first = true;
            foreach ($this->setters as $setter) {
                if ($first) {
                    $first = false;
                }
                else {
                    $str .= ", ";
                }
                $setter->getField()->assembleUsageSql($str, $parameterValues);
                $str .= "= ";
                $setter->getValue()->assembleUsageSql($str, $parameterValues);
            }
        }
    }
}
