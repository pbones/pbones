<?php

namespace pbones\query;

/**
 * 
 */
class Join {
    const INNER_JOIN = 'INNER JOIN';
    const LEFT_JOIN = 'LEFT JOIN';
    
    /**
     *
     * @var DbTable
     */
    public $table;
    /**
     *
     * @var QueryExpr 
     */
    public $onExpr;
    public $joinType;
    
    public function __construct(DbTable $model, QueryExpr $onExpr, $joinType)
    {
        $this->table = $model;
        $this->onExpr = $onExpr;
        $this->joinType = $joinType;
    }
}
