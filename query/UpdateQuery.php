<?php

namespace pbones\query;

/**
 */
class UpdateQuery extends JoinsAndWhereQueryBase
{
    private $setters = array();
    
    public function update(DbTable $model)
    {
        $this->firstTable = $model;
        return $this;
    }
    
    public function set(Field $field, $valueExpr)
    {
        array_push($this->setters, new Setter($field, $valueExpr));        
        return $this;
    }

    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        $str .= "UPDATE ";
        
        $this->assembleTableAndJoinsSql($str, $parameterValues);
        
        $str .= "SET ";
        
        $first = true;
        foreach ($this->setters as $setter) {
            if ($first) {
                $first = false;
            }
            else {
                $str .= ", ";
            }
            $setter->getField()->assembleUsageSql($str, $parameterValues);
            $str .= "= ";
            $setter->getValue()->assembleUsageSql($str, $parameterValues);
        }
        
        $this->assembleWhereSql($str, $parameterValues);
    }
}
