<?php

namespace pbones\query;

abstract class DbTable implements QueryElement 
{
    private $tableName;
    private $alias;
    
    public function __construct($tableName, $alias = null)
    {
        $this->tableName = $tableName;
        $this->alias = $alias;
    }
    
    public abstract function asAlias($alias);

    public function assembleDeclarationSql(&$str, array &$parameterValues)
    {
        $str .= "`" . $this->tableName . "` ";
        if ($this->alias !== null) {
            $str .= "AS `" . $this->alias . "` ";
        }
    }
    
    public function assembleUsageSql(&$str, array &$parameterValues)
    {
        if ($this->alias == null) {
            $str .= "`" . $this->tableName . "` ";
        }
        else {
            $str .= "`" . $this->alias . "` ";
        }
    }
}
