<?php

namespace pbones\query;

use PDO;
use PDOStatement;

/**
 */
abstract class QueryBase implements QueryElement
{
    
    public function assembleDeclarationSql(&$str, array &$parameterValues)
    {        
    }
    
    /**
     * Builds query text, gathers parameters and prepares PDO statement with 
     * bound parameter values
     * 
     * @param PDO $pdo
     * @return PDOStatement
     */
    public function getStatement(PDO $pdo)
    {
        $str = "";
        $params = array();
        $this->assembleUsageSql($str, $params);
        
        $stmt = $pdo->prepare($str);
        
        $i = 1;
        foreach ($params as $param) {
            $stmt->bindValue($i, $param, $param == null ? PDO::PARAM_NULL : PDO::PARAM_STR);
            $i++;
        }
        
        return $stmt;
    }
}
