<?php

namespace pbones\query;

/**
 */
abstract class JoinsAndWhereQueryBase extends QueryBase
{
    /**
     *
     * @var DbTable 
     */
    public $firstTable;
    /**
     *
     * @var array 
     */
    public $joins = array();
    /**
     * @var QueryExpr
     */
    public $whereExpr;    
    
    public function innerJoin(DbTable $model, QueryExpr $onExpr)
    {
        array_push($this->joins, new Join($model, $onExpr, Join::INNER_JOIN));
        return $this;
    }
    
    public function leftJoin(DbTable $model, QueryExpr $onExpr)
    {
        array_push($this->joins, new Join($model, $onExpr, Join::LEFT_JOIN));
        return $this;
    }
    
    public function where(QueryExpr $expr)
    {
        $this->whereExpr = $expr;
        return $this;
    }
    
    protected function assembleTableAndJoinsSql(&$str, array &$parameterValues) 
    {
        if ($this->firstTable !== null) {
            $this->firstTable->assembleDeclarationSql($str, $parameterValues);
        }
        
        foreach ($this->joins as $join) {
            $str .= $join->joinType . " ";
            $join->table->assembleDeclarationSql($str, $parameterValues);
            
            if ($join->onExpr !== null) {
                $str .= "ON ";
                $join->onExpr->assembleUsageSql($str, $parameterValues);
            }
        }
    }
    
    protected function assembleWhereSql(&$str, array &$parameterValues)
    {
        if ($this->whereExpr !== null) {
            $str .= "WHERE ";
            $this->whereExpr->assembleUsageSql($str, $parameterValues);
        }
    }
}
