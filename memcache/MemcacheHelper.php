<?php

namespace pbones\memcache;

use Memcache;

class MemcacheHelper
{
    private $host;
    private $port;
    private $compress;
    
    public function __construct($host, $port, $compress)
    {
        $this->host = $host;
        $this->port = $port;
        $this->compress = $compress;
    }
    
    public function getCache( $key = null )
    {
        if (! is_null( $key ) && is_string( $key ) ) {
            $memcache = new Memcache();
            $memcache->addServer( $this->host, $this->port);
            if ( $memcache->getServerStatus( $this->host, $this->port) == 1 && $memcache->connect( $this->host, $this->port) !== false ) {
                $cache = $memcache->get( $key );
                $memcache->close();
                if ( $cache === false ) {
                    return null;
                } else {
                    return $cache;
                }
            }
        }
        return false;
    }
    
    public function setCache( $key = null, $object = null, $expire = 2592000 )
    {
        if ( ! is_null( $key ) && is_string( $key ) ) {
            $memcache = new Memcache();
            $memcache->addServer( $this->host, $this->port);
            if ( $memcache->getServerStatus( $this->host, $this->port) == 1 && $memcache->connect( $this->host, $this->port) !== false ) {
                if ( $memcache->add( $key, $object, $this->compress, $expire ) ) {
                    $memcache->close();
                    return true;
                }
                $memcache->close();
            }
        }
        return false;
    }
    
    public function deleteCache( $key = null )
    {
        if ( ! is_null( $key ) && is_string( $key ) ) {
            $memcache = new Memcache();
            $memcache->addServer( $this->host, $this->port);
            if ( $memcache->getServerStatus( $this->host, $this->port) == 1 && $memcache->connect( $this->host, $this->port) !== false ) {
                $memcache->delete( $key );
                $memcache->close();
                return true;
            }
        }
        return false;
    }
}