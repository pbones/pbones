<?php

namespace pbones\api;

use pbones\converter\XmlConverter;

class ApiRouterApp
{    
    private $token = "5dadBAbcFe0CAfeA0A4c0CCfaaAfeEf";
    
    private $projectBaseDir = "";
    
    private $timer = false;
    private $request = array();
    private $config = array();
    private $error = false;
    private $route = array(
        'namespace' => null,
        'controller' => null,
        'method' => null,
        'parameters' => array(),
        'request_method' => null,
        'access_group' => null,
        'access_bit' => null,
        'user_access' => null
    );
    private $responseName = false;
    
    public function __construct($projectBaseDir)
    {
        $this->projectBaseDir = $projectBaseDir;
        // Set timer
        $this->timer = microtime( true );
        // Set data
        $this->request = array_merge( $_GET, $_POST );
        // Set route config
        $this->config = (object) $this->config;
        foreach ( ApiRouterAppConf::getRequest() as $key => $value ) {
            $this->config->$key = ( isset( $this->request[$value] ) ) ? $this->request[$value] : false;
        }
        // Set response name
        if ( $_FILES ) {
            reset($_FILES);
            $this->responseName = key($_FILES);
        } else {
            $this->responseName = ApiRouterAppConf::getResponse()->name;
        }
        // Set route config
        $this->initRoute();
    }
    
    
    public function route()
    {
        // Setting header
        if ( $this->config->format == 'json' ) {
            header('content-type: application/json; charset=utf-8');
        } else {
            header('content-type: application/xml; charset=utf-8');
        }
        // Checking the existence of the route object
        if ( $this->route === false ) {
            $this->error = ErrorCodes::UKNOWN_METHOD;
            return $this->errorResponse();
        }
        // Check request method
        if ( $this->route->request_method === false ) {
            $this->error = ErrorCodes::INVALID_REQUEST_METHOD;
            return $this->errorResponse();
        }
	// Check to access
	if ( $this->config->access_token !== $this->token ) {
		$this->error = ErrorCodes::REQUEST_DECLINED_BY_SERVER;
		return $this->errorResponse();
	}
        // Checking the existence of a class
        if ( ! class_exists( $this->route->namespace ) ) {
            $this->error = ErrorCodes::UKNOWN_METHOD;
            return $this->errorResponse();
        }
        $controller = new $this->route->namespace;
        // Checking the existence of a method
        if ( ! method_exists( $controller, $this->route->method ) ) {
            $this->error = ErrorCodes::UKNOWN_METHOD;
            return $this->errorResponse();
        }
        $resposne = call_user_func_array( array($controller, $this->route->method), (array) $this->route->parameters );
        $this->error = $controller->getError();
        if ( $resposne === false && $this->error > 0 ) {
            return $this->errorResponse();
        } else {
            return $this->response( $resposne, $controller );
        }
    }
    
    private function initRoute()
    {
        $controller = ( isset($this->config->controller) ) ? (string) $this->config->controller : false;
        $method = mb_strtolower( (string) $this->config->method, 'UTF-8' );
	if ( is_null( $controller ) || $controller === false || strlen( $controller ) == 0 ) {
		$this->route = false;
	} else if ( isset( ApiRouterAppConf::getMethods()->$controller->$method ) && file_exists( $this->projectBaseDir . str_replace( "\\", "/", ( (string) ApiRouterAppConf::getNamespace()->path ) ) . $controller . ApiRouterAppConf::getNamespace()->controller . ApiRouterAppConf::getNamespace()->extension ) ) {
            $this->route['namespace'] = (string) ApiRouterAppConf::getNamespace()->path . $controller . (string) ApiRouterAppConf::getNamespace()->controller;
            $this->route['controller'] = $controller . ApiRouterAppConf::getNamespace()->controller;
            $this->route['method'] = $method . (string) ( ( isset( ApiRouterAppConf::getNamespace()->method ) && ! is_object( ApiRouterAppConf::getNamespace()->method ) ) ? ApiRouterAppConf::getNamespace()->method : null );
            $this->route['parameters'] = array();
            if ( isset( ApiRouterAppConf::getMethods()->$controller->$method->request_method ) ) {
                if ( ApiRouterAppConf::getMethods()->$controller->$method->request_method == $this->config->request_method ) {
                    $this->route['request_method'] = true;
                } else {
                    $this->route['request_method'] = false;
                }
            } else {
                $this->route['request_method'] = true;
            }
            if ( isset( ApiRouterAppConf::getMethods()->$controller->$method->parameters ) && is_object( ApiRouterAppConf::getMethods()->$controller->$method->parameters ) ) {
                foreach ( ApiRouterAppConf::getMethods()->$controller->$method->parameters as $key => $value ) {
                    if ( isset( $this->request[$key] ) ) {
                        array_push( $this->route['parameters'], $this->request[$key] );
                    } else {
                        array_push( $this->route['parameters'], $this->serializeDataString( $value ) );
                    }
                }
            }
            if ( isset( ApiRouterAppConf::getMethods()->$controller->$method->mandatory ) && is_object( ApiRouterAppConf::getMethods()->$controller->$method->mandatory ) ) {
                foreach ( ApiRouterAppConf::getMethods()->$controller->$method->mandatory as $key => $value ) {
                    array_push( $this->route['parameters'], $this->serializeDataString( $value ) );
                }
            }
            $this->route = (object) $this->route;
        } else {
            $this->route = false;
        }
    }
    
    private function response( $data, $controller = null )
    {
        if ( $this->config->format == 'json' ) {
            if (!is_null($controller) && $controller->isCustomRender() === true) {
                return $controller->customRender($this->responseName, $data);
            }
            return json_encode( array( $this->responseName => $data ) );
        } else {
            $Xml = new XmlConverter();
            return $Xml->convert( $data, $this->responseName );
        }
    }
    
    private function errorResponse()
    {
        $error = ( $this->error > 0 ) ? $this->error : 1;
        if ( $this->config->format == 'json' ) {
            return json_encode( array( ApiRouterAppConf::getResponse()->error => ErrorAppConf::getInstance($error) ) );
        } else {
            $Xml = new XmlConverter();
            return $Xml->convert( ErrorAppConf::getInstance($error), ApiRouterAppConf::getResponse()->error );
        }
    }
    
    private function serializeDataString( $data = null )
    {
        if ( ! is_string( $data ) ) {
            $data = 'null';
        }
        $data = (string) mb_strtolower( $data, 'UTF-8' );
        if ( $data == 'array' ) {
            return array();
        }
        if ( $data == 'false' || $data == 'true' ) {
            return ( ( $data == 'true' ) ? true : false );
        }
        if ( $data == 'null' || strlen( trim($data) ) == 0 ) {
            return null;
        }
        if ( filter_var( $data, FILTER_VALIDATE_FLOAT ) === true ) {
            return (double) $data;
        }
        if ( filter_var( $data, FILTER_VALIDATE_INT ) === true ) {
            return (int) $data;
        }
        return (string) $data;
    }
    
} 
