<?php

namespace pbones\api;

abstract class ErrorCodes
{
        
    const UKNOWN_ERROR                      = "1";
    const UKNOWN_METHOD                     = "2";
    const UNDEFINED_ID                      = "3";
    const UNDEFINED_PID                     = "4";
    const REQUESTS_LIMIT_EXCEEDED           = "5";
    const REQUEST_DECLINED_BY_SERVER        = "6";
    const REQUEST_DECLINED_BY_USER          = "7";
    const VALUE_LEN_LESS_THAN_ALLOWED       = "8";
    const VALUE_LEN_GREATER_THAN_ALLOWED    = "9";
    const VALUE_IS_NULL                     = "10";
    const VALIDATION_FAILED                 = "11";
    const USER_ALREADY_EXISTS               = "12";
    const EMAIL_ALREADY_EXISTS              = "13";
    const USER_IS_NOT_APPROVED              = "14";
    const USER_IS_BLOCKED                   = "15";
    const USER_IS_DELETED                   = "16";
    const SESSION_ALREADY_EXISTS            = "17";
    const UNDEFINED_MIME_TYPE               = "18";
    const INVALID_REQUEST_METHOD            = "19";
    const APPLICATION_IS_NOT_APPROVED       = "20";
    const APPLICATION_IS_BLOCKED            = "21";
    const APPLICATION_IS_DELETED            = "22";
    const SESSION_HAS_EXPIRED               = "23";
    const UNDEFINED_USER_ID                 = "24";
    const USER_ACCOUNT_IS_EXPIRED           = "25";
    const USER_SUBSCRIPTION_IS_EXPIRED      = "26";
    const UNDEFINED_GROUP_ID                = "27";
    const GROUP_IS_NOT_APPROVED             = "28";
    const GROUP_IS_BLOCKED                  = "29";
    const GROUP_IS_DELETED                  = "30";
    const INCORRECT_MODEL_STATE             = "31";

}