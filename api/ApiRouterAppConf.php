<?php

namespace pbones\api;

class ApiRouterAppConf
{

    private static $apiRouterAppConf;
    
    private function __construct()
    {
        
    }
    
    private function __clone()
    {
         
    }
    
    private function __destruct()
    {
        
    }
    
    public static function getInstance()
    {
        if ( is_null( ApiRouterAppConf::$apiRouterAppConf ) ) {
            /** @todo need save and check memcached */
            if ( defined( "API_ROUTE_APP_CONF" ) && function_exists( "simplexml_load_file" ) ) {
                if ( file_exists( API_ROUTE_APP_CONF ) ) {
                    if ( ( $object = simplexml_load_file( API_ROUTE_APP_CONF ) ) !== false ) {
                        ApiRouterAppConf::$apiRouterAppConf = (object) json_decode( json_encode($object, true) );
                    }
                }
            }
        }
        return ApiRouterAppConf::$apiRouterAppConf;
    }
    
    public static function getRequest()
    {
        if ( isset( ApiRouterAppConf::getInstance()->request ) ) {
            return ApiRouterAppConf::getInstance()->request;
        }
        return false;
    }
    
    public static function getResponse()
    {
        if ( isset( ApiRouterAppConf::getInstance()->response ) ) {
            return ApiRouterAppConf::getInstance()->response;
        }
        return false;
    }
    
    public static function getNamespace()
    {
        if ( isset( ApiRouterAppConf::getInstance()->namespace ) ) {
            return ApiRouterAppConf::getInstance()->namespace;
        }
        return false;
    }
    
    public static function getMethods()
    {
        if ( isset( ApiRouterAppConf::getInstance()->methods ) ) {
            return ApiRouterAppConf::getInstance()->methods;
        }
        return false;
    }
}
