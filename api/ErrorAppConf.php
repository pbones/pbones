<?php

namespace pbones\api;

class ErrorAppConf
{
    
    private function __construct()
    {
        
    }
    
    private function __clone()
    {
        
    }
    
    private function __destruct()
    {
        
    }
    
    public static function getInstance( $code = null )
    {
        /** @todo need save and check memcached */
        if ( defined( "ERROR_APP_CONF" ) && function_exists( "simplexml_load_file" ) ) {
            if ( file_exists( ERROR_APP_CONF ) ) {
                if ( ( $object = simplexml_load_file( ERROR_APP_CONF ) ) !== false ) {
                    $object = (array) json_decode( json_encode($object, true) );
                    $object = (array) array_shift( $object );
                    $response = array();
                    for ( $i = 0; $i < count( $object ); $i++ ) {
                        if ( isset( $object[$i]->code ) ) {
                            $response[$object[$i]->code] = (array) $object[$i];
                        }
                    }
                    if ( is_null( $code ) ) {
                        return $response;
                    } else {
                        if ( isset( $response[$code] ) ) {
                            return $response[$code];
                        } else {
                            return null;
                        }
                    }
                }
            }
        }
        return false;
    }
    
}