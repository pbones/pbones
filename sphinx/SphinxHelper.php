<?php

namespace pbones\sphinx;
use SphinxClient;

class SphinxHelper
{
    private $host = null;
    private $port = null;
    private $timeout = 300;
    private $limit = 20;
    private $offset = 0;
    private $index = '*';
    private $rankingMode = SPH_RANK_PROXIMITY_BM25;
    private $rankingModeExp = null;
    private $sortMode = SPH_SORT_RELEVANCE;
    private $matchMode = SPH_MATCH_ALL;
    private $maxMatched = 0;
    private $arrayResult = true;
    private $cutoff = 0;
    
    public function __construct( $host = null, $port = null, $timeout = 300 )
    {
        $this->host = $host;
        $this->port = $port;
        $this->timeout = $timeout;
    }
    
    public function query( $query = null, $limit = null, $offset = null )
    {
        if ( is_null( $this->host ) || is_null( $this->port ) ) {
            return false;
        }
        if ( ! class_exists( 'SphinxClient' ) ) {
            return false;
        }
        if ( is_null( $query ) ) {
            return array();
        }
        
        $limit = ( is_numeric( $limit ) && $limit > 0 ) ? $limit : $this->limit;
        $offset = ( is_numeric( $offset ) && $offset >= 0 ) ? $offset : $this->offset;
        try {
            $sphinxClient = new SphinxClient();
            if ( @$sphinxClient->SetServer( $this->host, $this->port ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetConnectTimeout( $this->timeout ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetLimits( $offset, $limit, $this->maxMatched, $this->cutoff ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetArrayResult( $this->arrayResult ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetRankingMode( $this->rankingMode, $this->rankingModeExp ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetSortMode( $this->sortMode ) === false ) {
                return false;
            }
            if ( @$sphinxClient->SetMatchMode( $this->matchMode ) === false ) {
                return false;
            }
            if ( ( $response = @$sphinxClient->Query( $query, $this->index ) ) === false ) {
                return false;
            }
            return $response;
        } catch ( Exeptions $e ) {
            return false;
        }
        
    }
    
    /**
     * Set ranking mode
     * @param int $mode
     * @todo: Ranking modes:
     * SPH_RANK_PROXIMITY_BM25 - Default ranking mode which uses both proximity and BM25 ranking.
     * SPH_RANK_BM25 - Statistical ranking mode which uses BM25 ranking only (similar to most of other full-text engines). This mode is faster, but may result in worse quality on queries which contain more than 1 keyword.
     * SPH_RANK_NONE - Disables ranking. This mode is the fastest. It is essentially equivalent to boolean searching, a weight of 1 is assigned to all matches.
     */
    public function SetRankingMode( $mode = null, $exp = null )
    {
        $this->rankingMode = $mode;
        $this->rankingModeExp = $exp;
    }

    /**
     * Set matches sorting mode
     * @param int $mode
     * @todo: Sorting modes
     * SPH_SORT_RELEVANCE - Sort by relevance in descending order (best matches first).
     * SPH_SORT_ATTR_DESC - Sort by an attribute in descending order (bigger attribute values first).
     * SPH_SORT_ATTR_ASC - Sort by an attribute in ascending order (smaller attribute values first).
     * SPH_SORT_TIME_SEGMENTS - Sort by time segments (last hour/day/week/month) in descending order, and then by relevance in descending order.
     * SPH_SORT_EXTENDED - Sort by SQL-like combination of columns in ASC/DESC order.
     * SPH_SORT_EXPR - Sort by an arithmetic expression.
     */
    public function SetSortMode( $mode = null )
    {
        $this->sortMode = $mode;
    }
    
    /**
     * Set full-text query matching mode
     * @param int $mode
     * @todo: Match modes
     * SPH_MATCH_ALL - Match all query words (default mode).
     * SPH_MATCH_ANY - Match any of query words.
     * SPH_MATCH_PHRASE - Match query as a phrase, requiring perfect match.
     * SPH_MATCH_BOOLEAN - Match query as a boolean expression.
     * SPH_MATCH_EXTENDED - Match query as an expression in Sphinx internal query language.
     * SPH_MATCH_FULLSCAN - Enables fullscan.
     * SPH_MATCH_EXTENDED2 - The same as SPH_MATCH_EXTENDED plus ranking and quorum searching support.
     */
    public function SetMatchMode( $mode = null )
    {
        $this->matchMode = $mode;
    }
    
    /**
     * Set max matches, controls how much matches searchd will keep in RAM while searching.
     * @param int $mode
     */
    public function setMaxMatches( $mode = null )
    {
        $this->maxMatched = ( is_numeric( $mode ) && $mode >= 0 ) ? $mode : 0;
    }
    
    /**
     * Set cutoff, used for advanced performance control. It tells searchd to forcibly stop search query once cutoff matches have been found and processed.
     * @param int $mode
     */
    public function setCutoff( $mode = null )
    {
        $this->cutoff = ( is_numeric( $mode ) && $mode >= 0 ) ? $mode : 0;
    }
    
    /**
     * Change the format of result set array
     * @param boolean $mode
     */
    public function SetArrayResult( $mode = true )
    {
        $this->arrayResult = ( is_bool( $mode ) && $mode === false ) ? false : true;
    }
    
    /**
     * Set index
     * @param string $mode
     */
    public function setIndex( $mode = null )
    {
        $this->index = ( is_null( $mode ) || strlen( trim( $mode ) ) == 0 ) ? '*' : $mode;
    }
    
}